package mx.com.necsus;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;

public class FetchHBaseColumn {




    
    public static void main(String[] args) throws Exception {
        // Step 1: Set up HBase configuration
        Configuration config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", "zk-1,zk-2,zk-3"); // Change this to your HBase cluster's ZooKeeper quorum.
 



        try (Connection connection = ConnectionFactory.createConnection(config)) {

            String TABLE_NAME = "proveedores:accounts";
            Table table = connection.getTable(TableName.valueOf(TABLE_NAME));

            // Step 3: Retrieve data from HBase column
            String rowKey = "2BEIPWS0395K";
            String columnFamily = "df";
            String columnName = "sud4";

            Get get = new Get(Bytes.toBytes(rowKey));
            get.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName));

            org.apache.hadoop.hbase.client.Result result = table.get(get);

            byte[] valueBytes = result.getValue(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName));
            if (valueBytes != null) {
                String retrievedValue = Bytes.toString(valueBytes);
                System.out.println("Retrieved Value: " + retrievedValue);
            } else {
                System.out.println("Column not found.");
            }
        }
        
    }
}
