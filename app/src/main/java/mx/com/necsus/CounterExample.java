package mx.com.necsus;

public class CounterExample {
    public static void main(String[] args) {
        int maxCount = 100; // The maximum count value you want to reach

        for (int i = 0; i <= maxCount; i++) {
            System.out.print("Count: " + i + "\r");
            try {
                Thread.sleep(100); // Sleep for a short duration to make the update visible
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("\nCounting completed.");
    }
}