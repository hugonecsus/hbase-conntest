package mx.com.necsus;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class HBaseToJSON {

    public static Map<String, String> fieldSpec = new HashMap<String, String>();

    static {

        // Signup
        fieldSpec.put("sud1",  "{'type':'string','fieldName' : 'signUp.businessName' }");
        fieldSpec.put("sud2",  "{'type':'string','fieldName' : 'signUp.originCode' }");
        fieldSpec.put("sud3",  "{'type':'string','fieldName' : 'signUp.rfc' }");
        fieldSpec.put("sud4",  "{'type':'string','fieldName' : 'signUp.tin' }");
        fieldSpec.put("sud5",  "{'type':'string','fieldName' : 'signUp.legalEntityCode' }");
        fieldSpec.put("sud6",  "{'type':'string','fieldName' : 'signUp.names' }");
        fieldSpec.put("sud7",  "{'type':'string','fieldName' : 'signUp.firstSurName' }");
        fieldSpec.put("sud8",  "{'type':'string','fieldName' : 'signUp.secondSurName' }");
        fieldSpec.put("sud9",  "{'type':'string','fieldName' : 'signUp.position' }");
        fieldSpec.put("sud10", "{'type':'string','fieldName' : 'signUp.mobilePhoneCtryCode' }");
        fieldSpec.put("sud11", "{'type':'string','fieldName' : 'signUp.mobilePhone' }");
        fieldSpec.put("sud12", "{'type':'string','fieldName' : 'signUp.landLine' }");
        fieldSpec.put("sud13", "{'type':'string','fieldName' : 'signUp.email' }");
        fieldSpec.put("sud14", "{'type':'string','fieldName' : 'signUp.langApplication' }");

        // Account
        fieldSpec.put("acd1",  "{'type':'string','fieldName' : 'account.signUpDate' }");
        fieldSpec.put("acd2",  "{'type':'string','fieldName' : 'account.statusCode' }");
        fieldSpec.put("acd3",  "{'type':'string','fieldName' : 'accunt.statusDate' }");
        fieldSpec.put("acd4",  "{'type':'string','fieldName' : 'account.lastNoticeDate' }");
        fieldSpec.put("acd5",  "{'type':'string','fieldName' : 'account.noticeCount' }");
        fieldSpec.put("acd6",  "{'type':'string','fieldName' : 'account.certCongrats' }");
        fieldSpec.put("acd7",  "{'type':'string','fieldName' : 'account.postCertVersion' }");
        fieldSpec.put("acd8",  "{'type':'string','fieldName' : 'account.postCertStatus' }");
        fieldSpec.put("acd9",  "{'type':'date'  ,'fieldName' : 'account.postCertDate' }");
        fieldSpec.put("acd10", "{'type':'string','fieldName' : 'account.renewaAvailable' }");
        fieldSpec.put("acd11", "{'type':'string','fieldName' : 'account.isSearchable' }");
        fieldSpec.put("acd12", "{'type':'string','fieldName' : 'account.postCertFinishedDate' }");
        fieldSpec.put("acd13", "{'type':'string','fieldName' : 'account.hasPublicReach' }");
        fieldSpec.put("acd14", "{'type':'string','fieldName' : 'account.buyerAssociation' }");
        fieldSpec.put("acd15", "{'type':'string','fieldName' : 'account.latestPlan' }");
        fieldSpec.put("acd16", "{'type':'string','fieldName' : 'account.latestNoticeMonitoringType' }");


        // Payment
        fieldSpec.put("pyd1", "{'type':'string','fieldName' : 'payment.approverIdentifier' }");
        fieldSpec.put("pyd20", "{'type':'string','fieldName' : 'payment.captureDate' }");
        fieldSpec.put("pyd30", "{'type':'string','fieldName' : 'payment.captureRef' }");
        fieldSpec.put("pyd40", "{'type':'string','fieldName' : 'payment.isDeleted' }");
        fieldSpec.put("pyd50", "{'type':'string','fieldName' : 'payment.isConfirmed' }");
        fieldSpec.put("pyf10", "{'type':'string','fieldName' : 'payment.fileContent' }");
        fieldSpec.put("pyf20", "{'type':'string','fieldName' : 'payment.fileExtension' }");
        fieldSpec.put("pyf30", "{'type':'string','fieldName' : 'payment.fileSize' }");
        fieldSpec.put("pyf40", "{'type':'string','fieldName' : 'payment.uploadDate' }");
        fieldSpec.put("pys10", "{'type':'string','fieldName' : 'payment.statusCode' }");
        fieldSpec.put("pys20", "{'type':'string','fieldName' : 'payment.statusDate' }");
        fieldSpec.put("pyn10", "{'type':'string','fieldName' : 'payment.noteSize' }");

    }


    public static void main(String[] args) throws IOException {


        // Configuration and connection setup
        Configuration conf = HBaseConfiguration.create();
        conf.set("hbase.zookeeper.quorum", "zk-1,zk-2,zk-3");
        Connection connection = ConnectionFactory.createConnection(conf);
        TableName tableName = TableName.valueOf("proveedores:accounts"); // Replace with your table name
        Table table = connection.getTable(tableName);

        // Create a JSON object to hold row data
        JSONObject jsonRow;

        // Initialize a list to hold JSON objects for each row
        List<JSONObject> jsonRows = new ArrayList<>();

        // Initialize a FileWriter to write to the output file
        FileWriter fileWriter = null;

        // Scan the table
        Scan scan = new Scan();
        ResultScanner scanner = table.getScanner(scan);

        int rowCount = 0; // Initialize row count

        try {
            // Create the output file (if it doesn't exist) and open it for writing
            File outputFile = new File("output.json");
            fileWriter = new FileWriter(outputFile);

            // Iterate through the rows and columns
            for (Result result : scanner) {
                jsonRow = new JSONObject();

                // Extract row key
                String rowKey = Bytes.toString(result.getRow());
                jsonRow.put("rowKey", rowKey);

                // Iterate through the columns
                NavigableMap<byte[], NavigableMap<byte[], byte[]>> familyMap = result.getNoVersionMap();
                for (Map.Entry<byte[], NavigableMap<byte[], byte[]>> familyEntry : familyMap.entrySet()) {
                    String columnFamily = Bytes.toString(familyEntry.getKey());
                    NavigableMap<byte[], byte[]> qualifierMap = familyEntry.getValue();

                    for (Map.Entry<byte[], byte[]> qualifierEntry : qualifierMap.entrySet()) {
                        String qualifier = Bytes.toString(qualifierEntry.getKey());
                        byte[] valueBytes = qualifierMap.get(qualifierEntry.getKey());
                        
                        String value =  Bytes.toString(valueBytes);

                        System.out.println("Column: [" + qualifier + "]"  +  value);

                        
                        // Determine data type and encode binary columns in Base64
                        // String value = detectData(valueBytes);

                        // Add column to JSON object
                        jsonRow.put(columnFamily + ":" + qualifier, detectData(qualifier, valueBytes));
                    }
                }

                // Add JSON object for this row to the list
                jsonRows.add(jsonRow);

                // Write the JSON data for this row to the output file immediately
                fileWriter.write(jsonRow.toString(2) + "\n"); // Using 2 for pretty-printing and adding a newline
                

                // Print the row count
                updateProgressBar(rowCount, 1800);
  
                // Increment row count
                rowCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Close the FileWriter and resources
            if (fileWriter != null) {
                fileWriter.close();
            }
            scanner.close();
            table.close();
            connection.close();
        }

        System.out.println("Exported " + rowCount + " rows to output.json");
    }

    // Function to clear the console
    private static void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    private static void updateProgressBar(int completedRows, int totalRows) {

        // Clear the console
        clearConsole();

        // Calculate progress percentage
        double progress = (double) completedRows / totalRows;
        int progressBarWidth = 50; // Width of the progress bar
        int progressChars = (int) (progress * progressBarWidth);

        // Build and print the progress bar
        StringBuilder progressBar = new StringBuilder("[");
        for (int j = 0; j < progressBarWidth; j++) {
            if (j < progressChars) {
                progressBar.append("=");
            } else {
                progressBar.append(" ");
            }
        }
        progressBar.append("] ");
        progressBar.append(String.format("%.2f%%", progress * 100));
        System.out.print(progressBar.toString());

        // Add additional text if needed
        System.out.print(" - Row " + completedRows + " of " + totalRows);

    }

    private static String createJSOnData(String fieldName, byte[] data) throws JsonProcessingException, IOException {


        







        if ("string".equals(type)) {


            String[] fieldNames = fieldName.split("\\."); // Split fieldName by dot



            return  Bytes.toString(data);
        }



        // String value = Bytes.toString(data);

        // if ("0x00".equals(value.toLowerCase())) {
        //     return "false";
        // } else if ("0xFF".equals(value.toLowerCase())) {
        //     return "true";
        // }

    
        // Check for date patterns (you may need to refine this based on your date
        // formats)
    //    System.out.println("#######:" + value);
        // If none of the above conditions match, assume binary
        return Base64.getEncoder().encodeToString(data);
    }

    
}
