/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package mx.com.necsus;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;

public class App {
    public static void main(String[] args) throws IOException {
        Configuration config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", "zk-1,zk-2,zk-3");
        // config.set("hbase.zookeeper.property.clientPort", "2181");
        try (Connection connection = ConnectionFactory.createConnection(config)) {
          System.out.println("Connected ...");
          TableName[] tableNames = connection.getAdmin().listTableNames();
          for (TableName tableName : tableNames) {
            System.out.println(tableName.getNameAsString());
          }
        }
    }
}
