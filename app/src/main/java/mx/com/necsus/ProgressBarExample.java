package mx.com.necsus;

public class ProgressBarExample {
    public static void main(String[] args) {
        int totalTasks = 100; // Total number of tasks or items
        int completedTasks = 0; // Number of tasks completed

        // Loop to simulate progress
        for (int i = 0; i < totalTasks; i++) {
            // Perform some task
            // ...

            // Update completedTasks
            completedTasks++;

            // Clear the console
            clearConsole();

            // Calculate progress percentage
            double progress = (double) completedTasks / totalTasks;
            int progressBarWidth = 50; // Width of the progress bar
            int progressChars = (int) (progress * progressBarWidth);

            // Build and print the progress bar
            StringBuilder progressBar = new StringBuilder("[");
            for (int j = 0; j < progressBarWidth; j++) {
                if (j < progressChars) {
                    progressBar.append("=");
                } else {
                    progressBar.append(" ");
                }
            }
            progressBar.append("] ");
            progressBar.append(String.format("%.2f%%", progress * 100));
            System.out.print(progressBar.toString());

            // Add additional text if needed
            System.out.print(" - Task " + completedTasks + " of " + totalTasks);

            // Sleep for a short duration to simulate work
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("\nTask completed.");
    }

    // Function to clear the console
    public static void clearConsole() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
