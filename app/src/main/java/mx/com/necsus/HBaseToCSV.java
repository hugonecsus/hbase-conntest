

package mx.com.necsus;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellUtil;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.*;

import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;

public class HBaseToCSV {
    public static class HBaseMapper extends TableMapper<Text, Text> {
        @Override
        protected void map(ImmutableBytesWritable key, Result value, Context context)
                throws IOException, InterruptedException {
            // Get the row key
            String rowKey = Bytes.toString(key.get());

            // Iterate through all columns in the Result
            for (Cell cell : value.listCells()) {
                // Extract column family, qualifier, and value
                String columnFamily = Bytes.toString(CellUtil.cloneFamily(cell));
                String qualifier = Bytes.toString(CellUtil.cloneQualifier(cell));
                String columnValue = Bytes.toString(CellUtil.cloneValue(cell));

                // Emit row key, column family, column qualifier, and value
                String outputKey = rowKey + "," + columnFamily + "," + qualifier;
                context.write(new Text(outputKey), new Text(columnValue));
            }
        }
    }

    public static void main(String[] args) throws Exception {
        Configuration config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", "zk-1,zk-2,zk-3");
        // config.set("hbase.zookeeper.property.clientPort", "2181");
        
        Job job = Job.getInstance(config, "HBaseToCSV");
        job.setJarByClass(HBaseToCSV.class);
        Scan scan = new Scan();
        TableMapReduceUtil.initTableMapperJob("providers:accounts", scan, HBaseMapper.class, Text.class, Text.class, job);

        // Set the output format to TextOutputFormat to write to CSV files.
        job.setOutputFormatClass(TextOutputFormat.class);
        TextOutputFormat.setOutputPath(job, new Path("/tmp/"));

        System.exit(job.waitForCompletion(true) ? 0 : 1);
    }
}
